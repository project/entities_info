<?php

namespace Drupal\entities_info;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Service description.
 */
class EntitiesInfoGenerateTables implements EntitiesInfoGenerateTablesInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function createTableEntityFields(array $fields): array {
    $count = $this->t('Count items:') . $fields['count'];
    $label = $fields['label'];
    if (count($fields) === 2 && array_key_exists('count', $fields)) {
      return [
        '#name' => $label,
        '#count' => $count,
        '#markup' => '<p>' . $this->t('There is not fields created.') . '</p>',
      ];
    }

    unset($fields['count'], $fields['label']);
    $rows = $this->getTableRows($fields);

    return [
      '#type' => 'table',
      '#header' => $this->getTableHeaders(),
      '#rows' => $rows,
      '#name' => $label,
      '#count' => $count,
    ];
  }

  /**
   * Return table rows with field info.
   *
   * @param array $entity
   *   Entity with field configs.
   *
   * @return array
   *   Array with field info.
   */
  protected function getTableRows(array $entity): array {
    return array_map(function ($field) {
      return [
        $field['field_name'],
        $field['label'],
        $field['field_type'],
        $field['required'],
        $field['description'],
        $field['count_used'],
      ];
    }, $entity);
  }

  /**
   * Table headers.
   *
   * @return array
   *   Field information labels.
   */
  protected function getTableHeaders(): array {
    return [
      'field_name' => $this->t('Field name'),
      'label' => $this->t('Label'),
      'field_type' => $this->t('Field type'),
      'required' => $this->t('Required'),
      'description' => $this->t('Description'),
      'count_used' => $this->t('Count field used'),
    ];
  }

}
