# ENTITIES INFO EXPORT

## ABOUT

Entities Info Export allows you to display information from entities with FieldConfigs 
in table format. Shows the number of elements created for each bundle and the following 
information for each field created: Field name, Label, Field type, Required, Description,
and Number of fields used. You can export the information to a PDF file.

## INSTALLATION

Install as you would normally install a contributed Drupal module. See:
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules)
for further information.

## CONFIGURATION

Navigate to entities info export form in menu Configuration > Entities info export
(admin/config/entities_info), select the desired bundles and export info.
A button to export to PDF is available on the results page.

## SUPPORT

Please use the issue queue at <http://drupal.org/project/entities_info> for
all module support.

## MAINTAINERS

- Aaron Sanchez - [asanchezs](https://drupal.org/u/asanchezs)
